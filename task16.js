function variables(string)
{
    var len=string.length;
    var i;
    var final='';
    for(i=0;i<len;i++)
    {
        if(string[i]==='o' || string[i]==='O'||string[i]==='a'||string[i]==='A'||string[i]==='e'||string==="E" || string[i]==='i' || string[i]==="I" || string[i]==="u"||string[i]==="U")
        {
            final=final+string[i].toUpperCase();
        }
        else
        {
            final=final+string[i].toLowerCase();
        }
    }
    return(final);
}
module.exports=variables;
