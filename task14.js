function variables(sentence,character,flag)
{
    var index=sentence.indexOf(character);
    if(index===(-1))
    {
        return false;
    }
    if(flag===1)
    {
        var first=sentence.slice(0,index);
        return(first);
    }
    if(flag===2)
    {
        var last=sentence.slice(index+1);
        return(last);
    }
}
module.exports=variables;
