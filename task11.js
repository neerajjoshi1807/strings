function variables(number)
{
    var result='';
    var temp;
    if(number>0)
    {
        temp=number%10;

    }
    if(temp===1)
    {
        result=number.toString()+"st";
    }
    else if(temp===2)
    {
        result=number.toString()+"nd";
    }
    else if(temp===3)
    {
        result=number.toString()+"rd";
    }
    else
    {
        result=number.toString()+"th";
    }
    return(result);
}
module.exports=variables;
