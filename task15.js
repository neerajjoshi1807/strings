function variables(sentence,word)
{
    var index=sentence.indexOf(word);
    var len=word.length;
    if(index===(-1))
        return(false);
    var first=sentence.slice(0,index-1);
    var last=sentence.slice(index+len);
    var final=first+last;
    return(final);
}
module.exports=variables;
