function variables(str)
{
    var arr=[];
    var l=1;
    var m=1;
    for(var i=0;i<124;i++)
    {
        arr[i]=0;
    }
    for(var i=0;i<str.length;i++)
    {
        var temp=str.charCodeAt(i);
        if((temp>=97 && temp<=122))
        {
            arr[temp]=1;
        }
        if((temp>=65 && temp<=90))
        {
            arr[temp]=1;
        }
    }
    for(var j=97;j<=122;j++)
    {
        if(arr[j]===0)
        {
            l=0;
            break;
        }
    }
    for(var k=65;k<=90;k++)
    {
        if(arr[k]===0)
        {
            m=0;
            break;
        }
    }
    if(l===0 && m===0)
        return false;
    else
        return true;
}
module.exports=variables;
